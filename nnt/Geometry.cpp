#include "common.h"
#include "Parsers.h"

void parse_Geometry(FILE* sfile, int position, std::string file_name) {

	fseek(sfile, position, SEEK_SET);

	Parser* Geometry = new Parser(sfile, file_name, "Geometry", 416);

	Geometry->Skip(24);
	Geometry->GetInt("visualSkeletonSize");
	Geometry->readIntBody();
	Geometry->Skip(12); //vertexDeclarations

	Geometry->start("vertexDeclarations");
	Geometry->SkipData(8);
	Geometry->start("Item");

	Geometry->start("weights");
	Geometry->getVertexComponentType();
	Geometry->GetIntData("offset");
	Geometry->SkipData(4);
	Geometry->finish("weights");

	Geometry->start("texcoord1");
	Geometry->getVertexComponentType();
	Geometry->GetIntData("offset");
	Geometry->SkipData(4);
	Geometry->finish("texcoord1");

	Geometry->start("texcoord1");
	Geometry->getVertexComponentType();
	Geometry->GetIntData("offset");
	
	Geometry->finish("texcoord1");

	Geometry->GetIntData("stride");
	Geometry->SkipData(4);

	Geometry->start("position");
	Geometry->getVertexComponentType();
	Geometry->GetIntData("offset");
	Geometry->SkipData(4);
	Geometry->finish("position");

	Geometry->start("normal");
	Geometry->getVertexComponentType();
	Geometry->GetIntData("offset");
	Geometry->SkipData(4);
	Geometry->finish("normal");

	Geometry->start("indices");
	Geometry->getVertexComponentType();
	Geometry->GetIntData("offset");
	Geometry->SkipData(4);
	Geometry->finish("indices");

	Geometry->start("color");
	Geometry->getVertexComponentType();
	Geometry->GetIntData("offset");
	Geometry->SkipData(4);
	Geometry->finish("color");

	Geometry->finish("Item");
	Geometry->finish("vertexDeclarations");


	Geometry->GetBlob("vertexBuffer");
	Geometry->GetInt("version");
	Geometry->GetBoolMini("vb32");
	Geometry->GetBoolMini("useProceduralEffect");
	Geometry->GetBoolMini("useDecals");
	Geometry->GetBoolMini("useColors");

	Geometry->GetInt("startFrame");
	Geometry->GetInt("sourceFileCRC");

	Geometry->GetSortMode();
	Geometry->GetBlob("skeleton");

	Geometry->start("sceneNodes");
	Geometry->setDataPosiion(Parse_SceneNodes(Geometry->file, Geometry->getDataPosiion(), "sceneNodes", Geometry->getSubTableLen()));
	Geometry->finish("sceneNodes");

	Geometry->GetFloat("scaleDistanceStart");
	Geometry->GetFloat("scaleDistanceEnd");

	Geometry->Skip(4); //rootMaterial

	Geometry->GetInt("realSkeletonSize");
	Geometry->GetBool("portalModel");

	Geometry->Skip(16); //portalFragments

	Geometry->Skip(16); //parts

	Geometry->GetOrientationMode();

	Geometry->GetBool("optimizeMesh");

	Geometry->Skip(16); //occluderInfos

	Geometry->start("modelElements");
	Geometry->setDataPosiion(Parse_modelElements(Geometry->file, Geometry->getDataPosiion(), "modelElements", Geometry->getSubTableLen()));
	Geometry->finish("modelElements");

	Geometry->GetBool("lodModel");
	Geometry->GetFloat("lodFactor");

	Geometry->Skip(16); //lodDistances

	Geometry->GetBool("largeModel");
	
	Geometry->Skip(16); //joints

	Geometry->GetBlob("indexBuffer");
	Geometry->GetBool("ignoreZ");
	
	Geometry->GetHideRule();

	Geometry->GetInt("globalID");

	Geometry->Skip(16); //geometryFragments

	Geometry->GetAABB("geometryBox");
	Geometry->GetFloat("fogFactor");

	Geometry->Skip(16); //flareInfos
	
	Geometry->GetFloat("fadeDistanceStart");
	Geometry->GetFloat("fadeDistanceEnd");
	Geometry->GetBool("exportDefaultAnimation");
	Geometry->GetInt("endFrame");
	Geometry->GetBoolMini("enableDistanceFade");
	Geometry->GetBoolMini("decalModel");
	Geometry->GetBoolMini("castShadows");
	Geometry->Skip(1);
	Geometry->GetFileName("binaryFile");

	Geometry->Skip(16); //areaFragments

	Geometry->GetBool("absoluteDistanceFade");
	Geometry->GetAABB("aabb");
	Geometry->save();
	_getch();

}