#include "common.h"

struct TextureAtlas_xdb {
	int unk;
	int pos;
	int unk2;
	int unk3;
};

struct TextureAtlas_head {
	int unk1;
	int unk2;
	int unk3;
	int unk4;
	int unk5;
	int unk6;
	int unk7;
	int len1;
	int len2;
	int unk10;
	int unk11;
	int unk12;
} m_TextureAtlas_head;

struct TextureAtlas_Item {
	int unk1;
	int unk2;
	int unk3;
	int y;
	int unk5;
	int unk6;
	int x;
	int width;
	int unk9;
	int name_len;
	int unk11;
	int height;
} m_TextureAtlas_item;

void parse_TextureAtlas(FILE* sfile, char* data, int pos_data, char* file_name) {

	TextureAtlas_xdb* TextureAtlas_xdb_data = reinterpret_cast<TextureAtlas_xdb*>(data);
	fseek(sfile, TextureAtlas_xdb_data->pos + pos_data, SEEK_SET);

	std::string name;

	TextureAtlas_head *Head;
	Head = new TextureAtlas_head[1];
	fread(Head, sizeof(m_TextureAtlas_head), 1, sfile);
	char buffer;

	//char ss[255];
	//fread(&ss, sizeof(char), 255, sfile);
	int item_count = Head->len1 / 48;

	TextureAtlas_Item *item;
	item = new TextureAtlas_Item[item_count];
	fread(item, sizeof(m_TextureAtlas_head), item_count, sfile);

	name.append(file_name);
	std::string result = name.substr(name.find_last_of("\\") + 1, std::string::npos);
	std::string filename = name.substr(name.find_last_of("/") + 1, std::string::npos);
	name.resize(name.length() - filename.length());
	std::string filepath = "Out_TextureAtlas/" + name;
	std::string file_names = filepath + filename;

#ifndef _DEBUG
	boost::filesystem::create_directories(filepath.c_str());
#endif

	if (!FileExists(file_names.c_str())) {
		FILE* file = fopen(file_names.c_str(), "ab");

		fprintf(file, "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
		fprintf(file, "<TextureAtlas>\n");
		fprintf(file, "<sources>\n");

		for (size_t i = 0; i < item_count; i++)
		{
			fprintf(file, "<Item>\n");

			char file_name_one[255];
			for (int scx = 0;scx <= 255;scx++)
			{
				fread(&buffer, sizeof(char), 1, sfile);
				if (buffer == 0x0)
				{
					file_name_one[scx] = '\0';
					break;
				}
				file_name_one[scx] = buffer;

			}
			printf("file_name:%s\n\r", file_name_one);

			fprintf(file, "<name>%s</name>\n", file_name_one);
			fprintf(file, "<width>%i</width>\n", item[i].width);
			fprintf(file, "<height>%i</height>\n", item[i].height);
			fprintf(file, "<x>%i</x>\n", item[i].x);
			fprintf(file, "<y>%i</y>\n", item[i].y);

			fprintf(file, "</Item>\n");
		}

		fprintf(file, "</sources>\n");
		fprintf(file, "<combinedTexture href = \"%s#xpointer(/UITexture)\" / >\n", file_name);
		fprintf(file, "<inheritedSourceFile href = \"\" / >\n");
		fprintf(file, "</TextureAtlas>\n");
		fclose(file);


	}

}