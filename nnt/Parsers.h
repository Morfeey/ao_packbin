#pragma once
#include "common.h"
#include "OneTable.h"
#include "XdbOneTable.h"
class Parser
{
public:
	Parser(FILE* sfile, char* file_name, char* type, int body_len);
	Parser(FILE* sfile, std::string file_name, char* type, int body_len);
	Parser::Parser(FILE* file, int position, char* type, int body_len);
	~Parser();

	void Skip(int bytes);
	void SkipInt();
	void GetInt(std::string name);
	void GetFloat(std::string name);
	void GetFileName(std::string name);
	void GetAABB(std::string name);
	void GetStr(std::string name);
	void GetBool(std::string name);
	void GetBoolMini(std::string name);
	void GetBoolMini(std::string name, int skip);
	void GetBlob(std::string name);
	void PrintNullhref(std::string name);
	void PrintNullTag(std::string name);
	void PrintNameValue(std::string name, std::string value);
	void PrintNameValue(std::string name, int value);
	void finish(std::string name);

	void startItem(std::string name);
	void start(std::string name);
	void start(std::string name, std::string param);

	void GetSortMode();
	void GetOrientationMode();
	void GetHideRule();

	int save();

	//SomeFunctions
	void AnimationEvent();
	void VertexDeclaration();
	FILE* file;
	int getPosiion();
	int setDataPosiion(int pos);

	//Special
	int getSubTableLen();
	void GetTextureType();
	float readFloatBody();
	int readIntBody();
	int getDataPosiion();
	void GetVEC(std::string name);

	void GetIntData(std::string name);
	void GetFloatData(std::string name);
	void GetFloat5Data(std::string name);
	void PrintHref(std::string name, std::string value);
	void GetQuat(std::string name);
	void SkipData(int bytes);
	void SkipIntData();
	void getVertexComponentType();
	std::string GetString(int len);
	float readFloatData();
	int readIntData();
	FILE* sfile;
	std::string getFileNameOnPos(int position);

	int EndDataPosition = 0;
	int Lenght = 0;
	int BodyLen = 0;

	//Links
	void GetLinkTexture(std::string name);
	bool ChekSubDataBlockXDB();
private:
	FILE* data_file;
	int data_file_position;
	std::string file_name;
	char* type;
	boost::filesystem::path nfile;
	boost::filesystem::ofstream ofs;
	bool fsave = false;
};

std::string get_animation(int animation_id);
std::string get_texture_type(int id);

//SubTypes Parsers
int Parse_SceneNodes(FILE* file, int position, char* type, int body_len);
int Parse_modelElements(FILE* file, int position, char* type, int body_len);
int Parse_MaterialInstance(FILE* file, int position, char* type, int body_len);
int Parse_CommonMaterialParams(FILE* file, int position, char* type, int body_len);
int Parse_GeometryFragment(FILE* file, int position, char* type, int body_len);