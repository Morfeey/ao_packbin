#pragma once

struct AABB {
	float aabb_center_x;
	float aabb_center_y;
	float aabb_center_z;
	float aabb_extents_x;
	float aabb_extents_y;
	float aabb_extents_z;
} m_AABB;

struct QUAT {
	float x;
	float y;
	float z;
	float w;
} m_QUAT;

struct VEC3 {
	float x;
	float y;
	float z;
} m_VEC3;

struct subTable {
	int unk1;
	int len;
	int unk2;
	int unk3;
}m_subtable;

struct sstring {
	int unk1;
	int len;
	int len2;

} m_sstring;

struct aBLOB {
	int size;
	int localID;
	int unk1;
} m_BLOB;

struct sChekSubDataBlockXDB {
	int unk1;
	int unk2;
	int unk3;
	int unk4;
	int unk5;
	int unk6;

} m_sChekSubDataBlockXDB;
