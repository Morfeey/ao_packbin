#include "common.h"
#include "Parsers.h"

struct SkeletalAnimation_xdb {
	int unk;
	int pos;
	int unk2;
	int unk3;
};



void parse_SkeletalAnimation(FILE* sfile, int position, std::string file_name) {

	fseek(sfile, position, SEEK_SET);
	Parser* SkeletalAnimation = new Parser(sfile, file_name, "SkeletalAnimation", 168);
	SkeletalAnimation->Skip(24);
	SkeletalAnimation->GetInt("startFrame");
	SkeletalAnimation->GetFloat("speed");
	SkeletalAnimation->GetInt("sourceFileCRC");
	//SkeletalAnimation->GetStr("scriptName");

	SkeletalAnimation->Skip(12);
	SkeletalAnimation->GetInt("scriptID");

	SkeletalAnimation->GetBool("looped");
	SkeletalAnimation->GetFloat("fps"); 	
	SkeletalAnimation->GetInt("endFrame");
	SkeletalAnimation->GetFloat("blendTime");
	SkeletalAnimation->GetInt("binaryVersion");
	SkeletalAnimation->GetFileName("binaryFile");
	SkeletalAnimation->AnimationEvent();
	SkeletalAnimation->GetBlob("animation");
	SkeletalAnimation->GetAABB("aabbLastFrame");
	SkeletalAnimation->GetAABB("aabb");
	SkeletalAnimation->finish("SkeletalAnimation");
	SkeletalAnimation->save();
	delete(SkeletalAnimation);
}