#pragma once
#include "Table.h"

struct FileXdb {


	std::string name;
	int len = 0;
	bool status = false;
};

class XdbOneTable : public Table
{
public:
	XdbOneTable(): Table() {};
	void init(int position);
	~XdbOneTable();
	void ParseOneTable();
	int getPositionData();
	void ParseXdbBlock(int sposition, int count, int eposition);
	void ParseXdbData();
	std::map <int, FileXdb> files;
	static XdbOneTable* instance()
	{
		static XdbOneTable instance;
		return &instance;
	}

private:
	int data_block = 0;
	static XdbOneTable * p_instance;

};
#define sXdbOneTable XdbOneTable::instance()



void parse_CollisionMesh(FILE* sfile, int position, std::string file_name);
int parse_texture(FILE* sfile, int position, std::string file_name);
void parse_SkeletalAnimation(FILE* sfile, int position, std::string file_name);
void parse_UITexture(FILE* sfile, int position, std::string file_name);
void parse_Geometry(FILE* sfile, int position, std::string file_name);