#include "Parsers.h"

int Parse_MaterialInstance(FILE* file, int position, char* type, int body_len) {

	Parser* MaterialInstance = new Parser(file, position, "MaterialInstance", body_len);
	int MaterialInstanceCount = body_len / 52;
	MaterialInstance->Skip(12);

		MaterialInstance->GetBool("visible");
		MaterialInstance->GetFloat("vTranslateSpeed");
		MaterialInstance->GetBool("useFog");
		MaterialInstance->GetFloat("uTranslateSpeed");
		MaterialInstance->GetBool("transparent");

	return MaterialInstance->getDataPosiion();
}
