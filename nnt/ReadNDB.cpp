#include "common.h"

struct ndb_struct_table {
	int pos;
	int len;
	int unk;
} m_ndb_struct_table;

void Read_NDB(FILE * file, int position, int count) {
	fseek(file, position, SEEK_SET);
	printf("Position NDB: %i\n", ftell(file));
	ndb_struct_table *i_ndb_struct_table;
	i_ndb_struct_table = new ndb_struct_table[count];
	fread(i_ndb_struct_table, sizeof(m_ndb_struct_table), count, file);
	boost::filesystem::create_directories("Out/");
	boost::filesystem::path p{ "Out/Ndb_List.txt" };


	
	for (int cx = 0;cx != count;cx++)
	{
		char* buffer = new char[255];
		fseek(file, position + i_ndb_struct_table[cx].pos + (12 * cx), SEEK_SET);
		fread(buffer, sizeof(char), i_ndb_struct_table[cx].len, file);
		boost::filesystem::ofstream(p, std::ios_base::app) << buffer << "\n";
	}
}