#include "common.h"
std::string get_texture_type(int id)
{
	switch (id)
	{
	case 1:
		return("DXT3");

		break;
	case 2:
		return("DXT5");

		break;
	case 3:
		return("RGBA");

		break;
	case 4:
		return("L8");

		break;
	case 5:
		return("D24");

		break;
	case 6:
		return("D16");

		break;
	case 7:
		return("C565");

		break;
	case 8:
		return("C4444");

		break;
	case 9:
		return("RG16");

		break;
	case 10:
		return("R32F");

		break;
	case 11:
		return("RGBA16");

		break;
	default:
		return ("DXT1");

		break;
	}
}