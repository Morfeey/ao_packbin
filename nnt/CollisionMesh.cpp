#include "common.h"
#include "Parsers.h"
//FULL

struct CollisionMesh_xdb {
	int unk;
	int pos;
	int unk2;
	int unk3;
};


void parse_CollisionMesh(FILE* sfile, int position, std::string file_name)
{
	fseek(sfile, position, SEEK_SET);

	Parser* CollisionMesh = new Parser(sfile, file_name, "CollisionMesh", 104);

	CollisionMesh->Skip(24);
	CollisionMesh->GetInt("sourceFileCRC");
	CollisionMesh->Skip(8); //  defaultMaterial
	CollisionMesh->Skip(16); //  customMaterials
	CollisionMesh->GetInt("clipMask");
	CollisionMesh->GetInt("binaryVersion");
	CollisionMesh->GetFileName("binaryFile");
	CollisionMesh->GetAABB("aabb");
	CollisionMesh->PrintNullhref("defaultMaterial"); // WTF???? its ALL NULL ? NEED CHECK
	CollisionMesh->PrintNullTag("customMaterials");	 // WTF???? its ALL NULL ? NEED CHECK
	CollisionMesh->finish("CollisionMesh");	

	CollisionMesh->save();
}