#include "common.h"
std::string file_name = "C:\\ppack.bin";

FILE* read_file()
{
	FILE *pFile;
next:
	pFile = fopen(file_name.c_str(), "rb");

	if (pFile != NULL)
		return pFile;
	else
	{
		Sleep(5);
		goto next;
	}
}

bool FileExists(const char *fname)
{
	return _access(fname, 0) != -1;
}

std::string create_dir_on_patch(std::string name) {
	std::string result = name.substr(name.find_last_of("\\") + 1, std::string::npos);
	std::string filename = name.substr(name.find_last_of("/") + 1, std::string::npos);
	name.resize(name.length() - filename.length());
	std::string filepath = "Out/" + name;
	std::string file_names = filepath + filename;

	boost::filesystem::create_directories(filepath.c_str());
	return file_names;
}